# CarCar

Team:
```
* Liam - Service microservice
-Manufacturer React files
-Automobile React files

```


* Siah M - Auto Sales microservice
-vehicle models React files


## Design
-CarCar
```
CarCar is an app that allows the user to utilize a dealership's operations. It incapsulates 3 microservices the Inventory management system, the Service Management system, and the Sales management system. Each aspect of the dealership is detailed and fairly easy to use. With our architectural design for the microservices(backend server and frontend server handling their respective domains) the app is very responsive and optimized as of submission however, we intend on tweaking in javascript a bit more when there are no time constraints. It is at "MVP"(minimum viable product) level currently and can be cleaned up aesthetically and refactored functionality-wise. CarCar was the perfect opportunity for us to explore context mapping which we scribbled down on a whiteboard that pointed us in the right direction to begin our first lines of code. On a lower level, each microservice will interact with its own data in a bounded context. Liam and I decided that we were going to both need to interact with the inventory API via polling message handler in a bounded context. 
```

```
Create Models
-class models of native entities
-class model of Value object(interlopers) 

Create API views !RESTFUL!
-class views with "GET, POST, PUT, DELETE" for each model needing to retrieve or send requests.
-class encoders for each view to allow information to be translated through JSON files.

Create Poller message handler
-function to "GET" information from inventory microservice and then translate it into JSON then asign Automobile models ["vin"] to AutomobileVO models ["vin]

```

## Service microservice

```
Explain your models and integration with the inventory
microservice, here.

In the service microservice I created models to represent Technicians, Automobiles(as value objects from the inventory microservice) and the Appointments that they get assigned to. Therefor I gave the appointments a relationship to technicians since they would be a specific person that would be assigned to the appointment to service the automobile. I enabled a property called completed(default to false) on the appointment model as well to tract whether the appointment is completed. You can then update the completed property with a button that uses the "PUT" method to change it from False to True. I also enabled a property called is_vip on my Automobile model to indicate whether the automobile is coming from the inventory microservice(purchased from the dealership). This would mean if the concierge made an appointment with a VIN that did not belong to any AutomobileVO it would not get the is_vip property and subsequent features. The microservice service is fairly simple and lightweight but can be refactored more to clean up file sizes and make it slightly more readable.

```


## Sales microservice

```
I created 3 models based on the requirements for the sales services (SalesRep, Customer, AutoSales) along with one value object from inventory(AutomobileVO). I decided that an Auto Sale encompasses already established attributes (vehicle(AutomobileVO), sales rep, and customer). When an auto sale occurs all three must be included which is why I decided to make them ForeignKeys and to Protect on delete in the event an auto sale is "reversed"/"deleted".I implemented a search bar to assist with locating a sales rep by their name. This was done with a hook, I declared state the searched string, and filtered my auto sales list to include all strings matching the searched string. I would love to go back in and improve the look of the overall search bar and even add one to the vehicle models page.

After the project was in its final stages I decided to go back and apply more "realistic" Fields to my models for example: I updated the price attribute in AutoSale to DecimalField for an actual "dollar amount". Minor improvements can be made throughout the project in terms of fields however it is sufficient for the purposes of this assignment. 

What I did learn from this project is the importance of mapping beforehand and keeping track of attribute names. I ran into an issue where I refactored my attribute names but didn't quite get all of the instances. This caused a massive bug at the end which thankfully I was able to resolve with help<3. In addition, I did learn a lot of "smaller" things with this project regarding react (hooks especially).
```

import React from "react";


function ServiceAppointmentList({appointments}) {
    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date and Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Finished</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer_name}</td>
                <td>{appointment.date}</td>
                <td>{appointment.technician.name}</td>
                <td>{appointment.reason} </td>
              </tr>
            );
          })}
        </tbody>
      </table>  
    )
}
export default ServiceAppointmentList
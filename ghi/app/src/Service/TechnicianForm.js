import React from 'react';


class TechnicianForm extends React.Component {
    async
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employee_number: '',
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }

        const techniciansUrl = "http://localhost:8080/service/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        }
        const response = await fetch(techniciansUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            const cleared = {
                name: '',
                employee_number: '',
            };
            this.setState(cleared);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({ employee_number: value })
    }
    async componentDidMount() {
        const url = "http://localhost:8080/service/technicians/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            this.setState({ technician: data.technician });

        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a Technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" name="name" required type="text" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input value={this.state.employee_number} onChange={this.handleEmployeeNumberChange} placeholder="Employee number" name="employee_number" required type="number" id="employee_number" className="form-control" />
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>

                            <button className="btn btn-primary">Create a technician</button>
                        </form>
                    </div>
                </div>
            </div>

        );
    }

}


export default TechnicianForm;

import React, { useEffect, useState } from "react";



export default function ServiceHistoryForm({ appointments }) {

    let [searched, setSearched] = useState("");
    useEffect(() => {
    }, [])

    return (
        <>
            <div className="container">
                <div className="row">
                    <form id="form_search" name="form_search" method="get" action="" className="form-inline">
                        <div className="form-group">
                            <div className="input-group">
                                <input onChange={result => setSearched(result.target.value)} className="form-control" placeholder="Search a VIN" type="text" />
                            </div>
                        </div>
                    </form>
                </div>
                <h1>Service History</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date and Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Finished</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments && appointments
                            .filter(appointment => appointment.vin.includes(searched))
                            .map(appointment => {
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.customer_name}</td>
                                        <td>{appointment.date}</td>
                                        <td>{appointment.technician.name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{(appointment.completed) ? "true" : "false"}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
        </>
    )
}



import React from "react";

function ServiceAppointmentList({ appointments, updateAppointments }) {

  function Complete(id) {
    const fetchConfig = {
      method: "put",
      body: JSON.stringify({ "completed": "True" }),
      headers: { "Content-Type": "application/json" }
    }

    fetch(`http://localhost:8080/service/appointments/${id}/`, fetchConfig)
      .then(() => updateAppointments())
      .catch((e) => console.error(e));
  }

  function Cancel(id) {
    fetch(`http://localhost:8080/service/appointments/${id}/`, { method: "delete" })
      .then(() => updateAppointments())
      .catch((e) => console.error(e))
  }

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIP</th>
          <th>Is completed</th>
          <th>VIN</th>
          <th>Customer Name</th>
          <th>Date and Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th>Finished</th>
        </tr>
      </thead>
      <tbody>
        {appointments && appointments.map(appointment => {
          return (
            <tr key={appointment.id}>
              <td>{(appointment.vip) ? "true" : "false"}</td>
              <td>{(appointment.completed) ? "true" : "false"}</td>
              <td>{appointment.vin}</td>
              <td>{appointment.customer_name}</td>
              <td>{appointment.date}</td>
              <td>{appointment.technician.name}</td>
              <td>{appointment.reason} </td>
              <td>
                <button onClick={() => Cancel(appointment.id)}>Cancel</button>
                <button onClick={() => Complete(appointment.id)}>Complete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}
export default ServiceAppointmentList
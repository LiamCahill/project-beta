import React from 'react';

class AutomobileForm extends React.Component {
    async
    constructor(props) {
        super(props)
        this.state = {
            color: '',
            year: '',
            vin: '',
            model_id: '',
        }
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleYearChange = this.handleYearChange.bind(this)
        this.handleVinChange = this.handleVinChange.bind(this)
        this.handleModelChange = this.handleModelChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state }
        delete data.models

        console.log(data)
        const automobilelUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(automobilelUrl, fetchConfig)
        console.log(response)
        if (response.ok) {
            const newAutomobile = await response.json()
            const cleared = {
                color: '',
                year: '',
                vin: '',
                model_id: '',
            }
            this.setState(cleared)
        }
    }

    handleColorChange(event) {
        const value = event.target.value
        this.setState({ color: value })
    }

    handleYearChange(event) {
        const value = event.target.value
        this.setState({ year: value })
    }

    handleVinChange(event) {
        const value = event.target.value
        this.setState({ vin: value })
    }

    handleModelChange(event) {
        const value = event.target.value
        this.setState({ model_id: value })
    }

    render() {
        return (
            <div className="my-5 container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new Automobile</h1>

                            <form onSubmit={this.handleSubmit} id="create-automobile-form">
                                <div className="form-floating mb-3">
                                    <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" name="color" required type="text" id="color" className="form-control" />
                                    <label htmlFor="color">Color</label>
                                </div>

                                <div className="form-floating mb-3">
                                    <input value={this.state.year} onChange={this.handleYearChange} className="form-control" id="year" placeholder="Year" name="year" required type="number"></input>
                                    <label htmlFor="year">Year</label>
                                </div>

                                <div className="form-floating mb-3">
                                    <input value={this.state.vin} onChange={this.handleVinChange} className="form-control" id="vin" placeholder="Vin" name="vin" required type="text"></input>
                                    <label htmlFor="vin">Vin</label>
                                </div>

                                <div className="mb-3">
                                    <select value={this.state.model_id} onChange={this.handleModelChange} required id="model_id" name="model_id" className="form-select">
                                        <option value="">Choose a model</option>
                                        {this.props.models &&
                                            this.props.models.map(model => {
                                                return (
                                                    <option value={model.id} key={model.id}>
                                                        {model.name}
                                                    </option>
                                                )
                                            })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AutomobileForm

import React from "react";

function ModelsList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Picture URL</th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody>
        {props.vehiclemodels.models.map(model => {
          return (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td><img src={model.picture_url} alt="" width="20%" height="20%" /></td>
              <td>{ model.manufacturer.name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}

export default ModelsList;

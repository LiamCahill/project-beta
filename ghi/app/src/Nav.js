import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <NavLink className="nav-link" to='/models'>Show All Models</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to='/models/new'>Add Models</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/manufacturers/new">Add a Manufacturer</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/automobiles/new">Add Automobiles</NavLink>
            </li>


            <li>
              <NavLink className="nav-link" to="/technicians/new">Add a Technician</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/appointments/new">Make a service appointment</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/appointments/">List of service appointments</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/appointments/history/">Search for service history</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/autosales/">Show All Sales</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/autosales/new">Add Sale</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/autosales/history">Search sale by rep</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to='/salesreps/new'>Sales Rep</NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/customers/new">New customers</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

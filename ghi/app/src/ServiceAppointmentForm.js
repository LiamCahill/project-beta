import React from 'react';

class ServiceAppointmentForm extends React.Component { async
    constructor(props) {
        super(props)
        this.state = {
            customer_name: '',
            date:'',
            reason: '',
            vin: '',
            technician: '',
            technicians: [],
        }

        this.handleCustomerChange = this.handleCustomerChange.bind(this)
        this.handleDateChange = this.handleDateChange.bind(this)
        this.handleReasonChange = this.handleReasonChange.bind(this)
        this.handleVinChange = this.handleVinChange.bind(this)
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.technicians

        console.log(data)
        const appointmentUrl = 'http://localhost:8080/service/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            const newAppointment = await response.json()
            const cleared = {
                customer_name: '',
                date:'',
                reason: '',
                vin: '',
                technician: '',
            }
            this.setState(cleared)
        }
    }

    handleCustomerChange(event) {
        const value = event.target.value
        this.setState({customer_name: value})
    }

    handleDateChange(event) {
        const value = event.target.value
        this.setState({date: value})
    }

    handleReasonChange(event) {
        const value = event.target.value
        this.setState({reason: value})
    }

    handleVinChange(event) {
        const value = event.target.value
        this.setState({vin: value})
    }
    handleTechnicianChange(event) {
        const value = event.target.value
        this.setState({technician: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/service/technicians/';
    
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json()
        this.setState({technicians: data.technicians})
        }
    }

    render() {
    return (
        <div className="my-5 container">
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Set up a service appointment</h1>

            <form onSubmit={this.handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
                <input value={this.state.customer_name} onChange={this.handleCustomerChange} placeholder="Customer Name" name="customer_name" required type="text" id="customer_name" className="form-control"/>
                <label htmlFor="customer_name">Customer Name</label>
            </div>

            <div className="form-floating mb-3">
                <input value={this.state.date} onChange={this.handleDateChange} className="form-control" id="date" placeholder="Date" name="date" required type="text"></input>
                <label htmlFor="year">Date and Time</label>
            </div>


            <div className="form-floating mb-3">
                <input value={this.state.reason} onChange={this.handleReasonChange} className="form-control" id="reason" placeholder="Reason" name="reason" required type="text"></input>
                <label htmlFor="reason">Reason</label>
            </div>

            <div className="form-floating mb-3">
                <input value={this.state.vin} onChange={this.handleVinChange} className="form-control" id="vin" placeholder="Vin" name="vin" required type="text"></input>
                <label htmlFor="vin">VIN</label>
            </div>

        

            <div className="mb-3">
                <select value={this.state.technician} onChange={this.handleTechnicianChange} required id="technician" name="technician" className="form-select">
                <option value="">Choose a Technician</option>
                {this.state.technicians.map(technician => {
                        return (
                            <option value={technician.id} key={technician.id}>
                                {technician.name}
                            </option>
                        )
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create service appointment</button>
            </form>
        </div>
        </div>
    </div>
    </div>
    );
    }
}

export default ServiceAppointmentForm
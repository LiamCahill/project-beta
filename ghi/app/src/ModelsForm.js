import React from 'react'

class ModelsForm extends React.Component {
    async
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            picture_url: '',
            manufacturer: '',
            manufacturers: [],

        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePicture_URLChange = this.handlePicture_URLChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.manufacturers;
        data.manufacturer_id = data.manufacturer;
        data.picture_url = data.picture_url;
        delete data.manufacturer;
        delete data.pictureUrl;


        const modelsUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(modelsUrl, fetchConfig)
        if (response.ok) {
            const cleared = {
                name: '',
                picture_url: '',
                manufacturer: '',
            }
            this.setState(cleared)
        } else {
            console.log(response)
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handlePicture_URLChange(event) {
        const value = event.target.value;
        this.setState({ picture_url: value })
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value })
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/'; //manufacturer

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers }) // what to put
        }
    }

    render() {
        return (
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Vehicle Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-vehicle-model-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" name="name" required type="text" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.picture_url} onChange={this.handlePicture_URLChange} className="form-control" id="picture_url" placeholder="picture_url" name="picture_url" required type="url"></input>
                                <label htmlFor="picture_url">picture_url</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.manufacturer} onChange={this.handleManufacturerChange} name="manufacturer" className="form-select">
                                    <option value="">Vehicle Model Options</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option value={manufacturer.id} key={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModelsForm;

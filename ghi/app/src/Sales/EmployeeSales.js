import React from 'react';

class EmployeeSales extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      salesreps: [],
      autosales: [],
      salesrep: '',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  async handleChange(event) {
    const value = event.target.value;
    this.setState({ salesrep: value });

    const autosaleUrl = 'http://localhost:8090/sales/autosales/';
    const autosaleResponse = await fetch(autosaleUrl);

    if (autosaleResponse.ok) {
      const autosaleData = await autosaleResponse.json();

      if (this.state.salesrep === "") {
        this.setState({ autosales: autosaleData.autosales });
      } else {
        let EmployeeSales = [];
        for (const autosale of autosaleData.autosales) {
          if (autosale.salesrep.salesrep === this.state.salesrep) {
            EmployeeSales.push(autosale);
          }
        }
        this.setState({ autosales: EmployeeSales });
      }
    }
  }

  async componentDidMount() {
    const salesrepsUrl = 'http://localhost:8090/sales/salesreps/';
    const autosaleUrl = 'http://localhost:8090/api/autosales/';
    const salesrepsResponse = await fetch(salesrepUrl);
    const autosaleResponse = await fetch(autosaleUrl);

    if (salesrepsResponse.ok && autosaleResponse.ok) {
      const salesrepsData = await salesrepsResponse.json();
      const autosaleData = await autosaleResponse.json();

      this.setState({
        salesreps: salesrepsData.salesreps,
        autosales: autosaleData.autosales
      });
    }
  }

  render() {
    return (
      <div className="container">
        <p></p>
        <h2>Sales person history</h2>
        <div className="mb-3">
          <select onChange={this.handleChange} value={this.state.salesrep} required name="salesrep" id="salesrep" className="form-select">
            <option value="">Choose Sales Person</option>
            {this.state.salesreps.map(salesrep => {
              return (
                <option key={salesrep.salesrep} value={salesrep.salesrep}>
                  {salesrep.salesrep}
                </option>
              )
            })}
          </select>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.autosales.map(autosale => {
              return (
                <tr key={autosale.id}>
                  <td>{autosale.salesrep.salesrep}</td>
                  <td>{autosale.customer.customer_name}</td>
                  <td>{autosale.automobile.vin}</td>
                  <td>$ {new Intl.NumberFormat().format(autosale.price)}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default EmployeeSales;

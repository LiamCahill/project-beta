
from logging.config import IDENTIFIER
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json



from common.json import ModelEncoder
from .models import Appointment, AutomobileVO, Technician


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "is_vip"]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id"
    ]
    
class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
    "customer_name",
    "date",
    "reason",
    "technician",
    "id",
    "vin",
    "completed"
    ]
    encoders = {"technician": TechnicianEncoder(),}

    def get_extra_data(self, o):
        try:
            AutomobileVO.objects.get(vin=o.vin)
            return {"vip": True}
        except:
            return {"vip": False}
    
    # def get_extra_data(self, o):
    #     return {"vin": o.vin.vin}

@require_http_methods(["GET", "POST"])
def api_appointments_list(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = content["technician"]
            technician = Technician.objects.get(id=technician)
            content["technician"] = technician

            # automobile_vin = content["vin"]
            # automobile = AutomobileVO.objects.get(vin=automobile_vin)
            # content["vin"] = automobile
            
            # completed = content["completed"]
            # completed = Appointment.objects.get(completed=completed)
            # content["completed"] = completed

            appointments_list = Appointment.objects.create(**content)
            return JsonResponse(
                appointments_list,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except: 
            response = JsonResponse(
            {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE",])
def api_appointment_list(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(appointment,
            encoder=AppointmentEncoder,
            safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"appointment": "Does not exist"}) 
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"appointment": "Does not exist"})
    else:
        try:
            content =  json.loads(request.body)
            appointment = Appointment.objects.get(id=id)
            props = ["completed"]
            for prop in props:
                setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"appointment": "Does not exist"})
            response.status = 404
            return response



@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response





@require_http_methods(["GET"])
def service_list(request):
    if request.method == "GET":
        try:
            appointments_list = Appointment.objects.all()
            return JsonResponse(
                appointments_list,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



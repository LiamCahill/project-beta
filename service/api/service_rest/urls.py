from django.contrib import admin
from django.urls import path

from .views import api_appointments_list, api_technician, service_list, api_appointment_list
# from .views import 


urlpatterns = [
    path("appointments/",  api_appointments_list, name="api_appointments_list"),
    path("appointments/<int:id>/", api_appointment_list, name="api_appointment_list"),
    path("technicians/", api_technician, name="api_technician"),
    path("history/", service_list, name="service_list")
]
from email.policy import default
from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    is_vip = models.BooleanField(default=True)
    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Appointment(models.Model):
    customer_name = models.CharField(max_length=255)
    date = models.CharField(max_length=255)
    reason  = models.CharField(max_length=255)
    technician = models.ForeignKey(Technician, related_name="appointments", on_delete=models.PROTECT)
    vin = models.CharField(max_length=17)
    completed = models.BooleanField(default=False)
    

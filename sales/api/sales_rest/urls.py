from django.urls import path
from .views import CustomerListAPI, SalesRepListAPI, AutoSalesListAPI

urlpatterns = [
    path("autosales/", AutoSalesListAPI, name="AutoSalesListAPI"),
    path("customers/", CustomerListAPI, name="CustomerListAPI"),
    path("salesreps/", SalesRepListAPI, name="SalesRepListAPI"),
]

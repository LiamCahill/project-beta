from django.db import IntegrityError
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutoSale, AutomobileVO, Customer, SalesRep

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin"]


class SalesRepListEncoder(ModelEncoder):
    model = SalesRep
    properties = ["id", "employee_name", "employee_id"]


class SalesRepDetailEncoder(ModelEncoder):
    model = SalesRep
    properties = ["id", "employee_name", "employee_id"]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "customer_name", "address", "phone_number"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "customer_name", "address", "phone_number"]


class AutoSaleListEncoder(ModelEncoder):
    model = AutoSale
    properties = [
        "id",
        "price",
        "vehicle",
        "salesrep",  # employee_name
        "customer"
    ]

    encoders = {
        "vehicle": AutomobileVODetailEncoder(),
        "salesrep": SalesRepListEncoder(),  # employee_name
        "customer": CustomerListEncoder(),
    }

class AutosaleDetailEncoder(ModelEncoder):
    model = AutoSale
    properties = [
        "id",
        "price",
        "vehicle",
        "salesrep",  # employee_name
        "customer"
    ]

    encoders = {
        "vehicle": AutomobileVODetailEncoder(),
        "salesrep": SalesRepListEncoder(),  # employee_name
        "customer": CustomerListEncoder(),
    }

@require_http_methods(["GET","POST"])
def SalesRepListAPI(request):
    if request.method == "GET":
        salesreps = SalesRep.objects.all()
        return JsonResponse(
            {"salesreps": salesreps},
            encoder=SalesRepListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesrep = SalesRep.objects.create(**content)
        return JsonResponse(
            salesrep,
            encoder=SalesRepDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def CustomerListAPI(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse({
            'customer': customer},
            encoder=CustomerDetailEncoder,
            safe=False,)

@require_http_methods(["GET", "POST"])
def AutoSalesListAPI(request):
    if request.method == "GET":
        autosales = AutoSale.objects.all()
        return JsonResponse(
            {"autosales": autosales},
            encoder=AutoSaleListEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.get(customer_name=content["customer"])
        salesrep = SalesRep.objects.get(employee_name=content["salesrep"])
        automobile = content["vehicle"]
        automobile = AutomobileVO.objects.get(vin=automobile)
        content["vehicle"] = automobile
        content["salesrep"] = salesrep
        content["customer"] = customer
    autosale = AutoSale.objects.create(**content)
    return JsonResponse(
        autosale,
        encoder=AutosaleDetailEncoder,
        safe=False,
    )
